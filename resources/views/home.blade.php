@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="raw pb-3">
                        <a href="{{ url('drop') }}" class="btn btn-primary btn-sm">Upload Config</a>
                    </div>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Title</th>
                          <th scope="col">Type</th>
                          <th scope="col">Size</th>
                          <th scope="col">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($files as $file)
                        <tr>
                          <th scope="row">1</th>
                          <td>{{ $file->file_title }}</td>
                          <!-- <td>
                              <a href="{{ url('drop/'. $file->file_title) }}">{{ $file->file_title }}</a>
                          </td> -->
                          <td>{{ $file->file_type }}</td>
                          <td>{{ number_format($file->file_size / 1024, 1) }} Kb</td>
                          <td>
                              <a href="{{ url('drop/'. $file->file_title . '/download') }}" class="btn btn-success btn-sm">Download</a>
                                            <a href="{{ url('drop/'. $file->id . '/delete') }}" class="btn btn-danger btn-sm">Delete</a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
@stop