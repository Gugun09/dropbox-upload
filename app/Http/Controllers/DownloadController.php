<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Dropfile;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();
    }

    public function index()
    {
        $files = Dropfile::latest()->paginate(1);
        
        return view('config.index', compact('files'));
    }

    public function download($fileTitle)
    {
    	try {
    		return Storage::disk('dropbox')->download('public/upload/' . $fileTitle);
    	} catch (Exception $e) {
    		return abort(404);
    	}
    }
}
